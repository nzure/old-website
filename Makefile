OUT=./build/~nzure

all: assets site

assets:
	sass --no-source-map -c ./src/style.scss ${OUT}/style.css
	cp ./src/nzure.jpg ${OUT}/nzure.jpg

site: src
	soupault

serve:
	python -m http.server --directory ./build 8000

deploy:
	rsync -rtvzP ${OUT}/ nzure@leaft.ch:~/www
